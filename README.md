# OSGI plugins with IntelliJ

A sample of OSGI project in IntelliJ.

## Prerequisites 

#### Apache Felix
Felix is an example of instance of OSGi. <br>
If you wish to use Felix, you need to [download](https://felix.apache.org/documentation/getting-started.html) it. <br>
You will configure it in the step 2 on the next section.

## How to use it?

#### Step 1: import it in IntelliJ
You can directly open the folder in IntelliJ !

#### Step 2: create an OSGi framework instance
After unzipping the OSGi distribution you previously downloaded (Felix or other), follow these instructions:
+ Go to `File > Settings > Languages & Frameworks > OSGi`
+ Click on `+` to add a new Felix (or other) instance
+ In `Home directory`, select the unzipped folder of the distribution
+ Go to `Run > Edit configurations`
+ In `OSGi framework`, select your new OSGi instance. Your bundle is ready to run !

#### Step 3: run the bundle
By running `OSGi Bundle`, you will launch the application modules. <br>
> By default, the OSGi console is activated. You can disable it in `Run > Edit configurations > Additional Framework Properties`.

## How to create a module?

+ Go to `File > New > Module...`
+ In `Additional Libraries and Frameworks`, select OSGi
+ Click on `Next` and give a name to your module.
+ Go to `File > Project Structure... > Modules`
+ Below your new module, click on `OSGi` and go to `Manifest Generation`
+ Give a symbolic name to your module !
+ Go to `Run > Edit configurations`
+ Add your new module to the start list !
> If your module has to be started after all others, you can increase its start level. Don't forget to increase the Framework start level too !

![Plugin creation](doc/create-plugin.gif)
